﻿using UnityEngine;

namespace Game.Extensions {
    /// <summary>
    /// Handles mathematical extensions.
    /// </summary>
    public static class EMath {
        /// <summary>
        /// Returns travel time with distance/speed.
        /// </summary>
        public static float GetTimeBySpeed(Vector3 start, Vector3 end, float speed) {
            return (end - start).magnitude / speed;
        }
        public static float Normalize(float value, float min = 0.0f, float max = 1.0f) {
            return Mathf.Min(Mathf.Max(value, min), max);
        }
        public static int Normalize(int value, int min, int max) {
            return Mathf.Min(Mathf.Max(value, min), max);
        }
        public static float CutInt(this float val) {
            return val - (int)val;
        }
        public static float Cycled(float value, float min, float max) {
            float range = max - min;
            float distWithoutInt = CutInt((value - min) / range);
            return (range * (distWithoutInt < 0 ? 1f + distWithoutInt : distWithoutInt)) + min;
        }
        public static string FormatNumber(int value, int minDigitsCount, string valueSpaceHolder = "0") {
            var valueString = value.ToString();
            string fixer = "";
            for (int i = 0; i < minDigitsCount - valueString.Length; ++i) {
                fixer += valueSpaceHolder;
            }
            return fixer + valueString;
        }
        public static string GetLengthAsString(int value, string seperator = ":", string valueSpaceHolder = "0", bool cutLessDigits = false, int minDividingLevel = 1) {
            int minDigits = cutLessDigits ? 1 : 2;
            int iterations = 0;
            int tempVar = value;
            string lengthString = FormatNumber(tempVar % 60, minDigits, valueSpaceHolder);
            while (tempVar > 59 || iterations < minDividingLevel) {
                lengthString = FormatNumber((tempVar /= 60) % 60, minDigits, valueSpaceHolder) + seperator + lengthString;
                iterations++;
            }
            return lengthString;
        }
    }
}
