﻿using System.Collections.Generic;

namespace Game.Extensions {
    public static class EDictinary {
        public static TValue GetValue<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue defaultValue = default(TValue)) {
            TValue outValue;
            if (dict.TryGetValue(key, out outValue)) {
                return outValue;
            }
            return defaultValue;
        }
    }
}
