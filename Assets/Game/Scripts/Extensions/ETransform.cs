﻿using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;

namespace Game.Extensions {
    public static class ETransform {
        /// <summary>
        /// Fade and activate all children (Fades sprite renderer to endValue, then sets alpha to startValue and deactivates gameObject)
        /// </summary>
        public static void FadeToActiveRecursively(this Transform transform, float endValue, float duration, bool active) {
            foreach (Transform childTransform in transform) {
                childTransform.FadeToActiveRecursively(endValue, duration, true);
            }
            var sr = transform.GetComponent<SpriteRenderer>();
            if (sr) {
                float startValue = sr.color.a;
                sr.DOFade(endValue, duration).OnComplete(delegate {
                    var color = sr.color;
                    color.a = startValue;
                    sr.color = color;
                    sr.gameObject.SetActive(active);
                });
            }
        }
        /// <summary>
        /// Fade and activate all children (Fades sprite renderer to endValue, then sets alpha to startValue and deactivates gameObject)
        /// </summary>
        public static Tween FadeRecursively(this Transform transform, float endValue, float duration, float? startValue = null) {
            var srStack = new Stack<SpriteRenderer>();
            transform.GetComponentsInChildrenRecursively(ref srStack);
            var spriteRenderer = transform.GetComponent<SpriteRenderer>();
            if (spriteRenderer) {
                srStack.Push(spriteRenderer);
            }
            while (srStack.Count != 0) {
                var sr = srStack.Pop();
                float sValue = startValue != null ? startValue.Value : sr.color.a;

                var color = sr.color;
                color.a = sValue;
                sr.color = color;

                sr.DOFade(endValue, duration);
            }
            return DOVirtual.DelayedCall(duration, null, false);
        }
        /// <summary>
        /// Raises order of sprite renderer on each object in children recoursevly by given delta in orders.
        /// </summary>
        public static void ChangeOrder(this Transform objectTransform, int changeDelta) {
            int childCount = objectTransform.childCount;
            if (childCount > 0) {
                for (int i = 0; i < childCount; i++) {
                    ChangeOrder(objectTransform.GetChild(i), changeDelta);
                }
            }
            var spriteRenderer = objectTransform.GetComponent<SpriteRenderer>();
            if (spriteRenderer != null) {
                spriteRenderer.sortingOrder += changeDelta;
            }
        }
        /// <summary>
        /// Method is void to improve perfomance of using reference to components stack.
        /// </summary>
        public static void GetComponentsInChildrenRecursively<T>(this Transform transform, ref Stack<T> childrens) where T : Component {
            int childCount = transform.childCount;

            for (int i = 0; i < childCount; i++) {
                var children = transform.GetChild(i);
                var component = children.GetComponent<T>();
                children.GetComponentsInChildrenRecursively(ref childrens);
                if (component) {
                    childrens.Push(component);
                }
            }
        }
        /// <summary>
        /// Finds gameObject by name part in children (recursively).
        /// </summary>
        public static Transform FindByNamePartInChildren(this Transform transform, string nameContains) {
            Transform foundTransform;
            int count = transform.childCount;
            Stack<Transform> toPeekInChildren = new Stack<Transform>();
            for (int i = 0; i < count; i++) {
                var child = transform.GetChild(i);
                if (child.name.Contains(nameContains)) {
                    return child;
                }
                toPeekInChildren.Push(child);
            }
            while (toPeekInChildren.Count != 0) {
                foundTransform = FindByNamePartInChildren(toPeekInChildren.Pop(), nameContains);
                if (foundTransform) {
                    return foundTransform;
                }
            }
            return null;
        }
        /// <summary>
        /// Finds gameObject by name part to lower in children (recursively).
        /// </summary>
        public static Transform FindByNamePartToLowerInChildren(this Transform transform, string nameContainsToLower) {
            Transform foundTransform;
            int count = transform.childCount;
            Stack<Transform> toPeekInChildren = new Stack<Transform>();
            for (int i = 0; i < count; i++) {
                var child = transform.GetChild(i);
                if (child.name.ToLower().Contains(nameContainsToLower)) {
                    return child;
                }
                toPeekInChildren.Push(child);
            }
            while (toPeekInChildren.Count != 0) {
                foundTransform = FindByNamePartToLowerInChildren(toPeekInChildren.Pop(), nameContainsToLower);
                if (foundTransform) {
                    return foundTransform;
                }
            }
            return null;
        }
        /// <summary>
        /// Change sprite sorting order with value delta but without ingored elements.
        /// </summary>
        public static void ChangeOrderRecursively(this Transform transform, int value) {
            foreach (Transform childTransf in transform) {
                childTransf.ChangeOrderRecursively(value);
            }
            var sprRend = transform.GetComponent<SpriteRenderer>();
            if (sprRend) {
                sprRend.sortingOrder += value;
            }

        }
        /// <summary>
        /// Change sprite sorting order with value delta and ingored elements.
        /// </summary>
        public static void ChangeOrderRecursively(this Transform transform, int value, ref List<Transform> ignoreList) {
            foreach (Transform childTransform in transform) {
                if (ignoreList.Contains(childTransform)) {
                    continue;
                }
                childTransform.ChangeOrderRecursively(value);
            }
            var spriteRenderer = transform.GetComponent<SpriteRenderer>();
            if (spriteRenderer) {
                spriteRenderer.sortingOrder += value;
            }
        }
        /// <summary>
        /// Motion by a parabolic trajectory.
        /// </summary>
        public static IEnumerator<Transform> ProjectileMotion(this Transform transform, Vector2 targetPos, Vector2 offset, bool changeScale, System.Action nextAction) {
            float xDistance, yDistance;
            xDistance = targetPos.x - transform.position.x;
            yDistance = targetPos.y - transform.position.y;

            float firingAngle = Mathf.Atan2(yDistance, xDistance) * Mathf.Rad2Deg;

            float totalVelocity;
            totalVelocity = xDistance / (Mathf.Sin(2 * firingAngle * Mathf.Deg2Rad) / Physics2D.gravity.magnitude);

            float xVelo, yVelo;
            xVelo = Mathf.Sqrt(totalVelocity) * Mathf.Cos(firingAngle * Mathf.Deg2Rad) + offset.x;
            yVelo = Mathf.Sqrt(totalVelocity) * Mathf.Sin(firingAngle * Mathf.Deg2Rad) + yDistance + offset.y;

            float flightDuration = xDistance / xVelo;

            if (changeScale) transform.DOScale(new Vector2(0.6f, 0.6f), 1.2f).OnComplete(delegate {
                transform.localScale = Vector2.one * 0.25f;
            });

            float elapseTime = 0f;

            while (elapseTime < flightDuration) {
                transform.Translate(xVelo * Time.deltaTime, (yVelo - Physics2D.gravity.magnitude * elapseTime) * Time.deltaTime, 0f);
                elapseTime += Time.deltaTime;

                yield return null;
            }
            if (nextAction != null) nextAction();
        }
    }

}