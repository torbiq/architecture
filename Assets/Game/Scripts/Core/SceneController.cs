﻿#define USE_DEBUG_MODE

using DG.Tweening;
using Game.Extensions;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SceneController : Singletone<SceneController> {
    public float fadeInDelay = 1f;
    public float fadeOutDelay = 1f;
#if USE_DEBUG_MODE
    public float logDelay = 0.1f;
#endif
    public Canvas fadeCanvas;
    public Image fadeImage;
    public Image progressBarBg;
    public Image progressBarImage;

    public event Action<Level, Level> OnStartTransition;
    public event Action<Level> OnLevelLoaded;
    public event Action<Level> OnLevelActivated;

    private AsyncOperation _loadingLevelAsync;
    private List<ILevelAffector> _levelAffectorsAll = new List<ILevelAffector>();
    private List<ILevelAffector> _quedAffectors = new List<ILevelAffector>();
    private Level _currentLevel;
    private readonly Dictionary<Level, LvlGroup> _levelGroups = new Dictionary<Level, LvlGroup> {
        { Level.EntryPoint, LvlGroup.Menu },
        { Level.LoginPage, LvlGroup.Menu },
        { Level.RateMeScene, LvlGroup.Menu },
        { Level.TestMap, LvlGroup.Shop },
        { Level.TestGarage, LvlGroup.Shop },
    };
    private readonly LvlGroup _defaultLevelGroup = LvlGroup.Menu;

    public static Level GetCurrentLevelFromUnity() {
        return (Level)System.Enum.Parse(typeof(Level), SceneManager.GetActiveScene().name);
    }
    public Level GetCurrentLevel() {
        return _currentLevel;
    }

    protected override void Init() {
        _currentLevel = GetCurrentLevelFromUnity();

        var color = fadeImage.color;
        color.a = 0f;
        fadeImage.color = color;

        fadeCanvas.gameObject.SetActive(false);
        progressBarBg.gameObject.SetActive(false);

        progressBarImage.fillAmount = 0f;
    }
    public LvlGroup GetLevelGroup(Level level) {
        return _levelGroups.GetValue(level, _defaultLevelGroup);
    }
    public bool IsInTransition() {
        return fadeCanvas.gameObject.activeSelf;
    }
    public void ChangeScene(Level scene) {
        if (IsInTransition()) {
            Log.Error("Transition is running.");
            return;
        }
        if (OnStartTransition != null) {
            OnStartTransition(_currentLevel, scene);
        }
        FadeIn().OnComplete(() => {
            StartCoroutine(LoadSceneAsync(scene, OnProgressUpdate, () => { FadeOut(); }));
        });
    }
    public bool CanActivateLevel(Level level) {
        for (int i = 0, count = _quedAffectors.Count; i < count; i++) {
            if (!_quedAffectors[i].CanActivateLevel(level)) {
                return false;
            }
        }
        if (_loadingLevelAsync != null) {
            _loadingLevelAsync.allowSceneActivation = true;
        }
        PauseController.Instance.Unpause();
        return true;
    }

    private Tween FadeIn() {
        fadeCanvas.gameObject.SetActive(true);
        progressBarImage.fillAmount = 0f;
        fadeImage.DOKill(false);
        DOVirtual.DelayedCall(fadeInDelay, () => { progressBarBg.gameObject.SetActive(true); }, false);
        return fadeImage.DOFade(1.0f, fadeInDelay);
    }
    private Tween FadeOut() {
        progressBarBg.gameObject.SetActive(false);
        fadeImage.DOFade(0.0f, fadeOutDelay);
        return DOVirtual.DelayedCall(fadeOutDelay, () => { fadeCanvas.gameObject.SetActive(false); }, false);
    }
    private void OnProgressUpdate(float progress) {
        progressBarImage.fillAmount = progress;
    }
    private IEnumerator LoadSceneAsync(Level scene, Action<float> OnProgressUpdate, Action OnCompleteCallback = null) {
        PauseController.Instance.Pause();
        _loadingLevelAsync = SceneManager.LoadSceneAsync(scene.ToString(), LoadSceneMode.Single);
        _loadingLevelAsync.allowSceneActivation = true;
        if (OnProgressUpdate != null) {
            while (!_loadingLevelAsync.isDone) {
                OnProgressUpdate(_loadingLevelAsync.progress);
#if USE_DEBUG_MODE
                //yield return new WaitForSeconds(logDelay);
                yield return null;
                Log.Msg("Loading level progress " + (_loadingLevelAsync.progress * 100.0f).ToString("0.0") + "%...");
#else
                yield return null;
#endif
            }
            OnProgressUpdate(_loadingLevelAsync.progress);
            yield return null;
        }
        else {
            while (!_loadingLevelAsync.isDone) {
#if USE_DEBUG_MODE
                //yield return new WaitForSeconds(logDelay);
                yield return null;
                Log.Msg("Loading level progress " + (_loadingLevelAsync.progress * 100.0f).ToString("0.0") + "%...");
#else
                yield return null;
#endif
            }
        }
        if (OnLevelLoaded != null) {
            OnLevelLoaded(scene);
        }
        if (OnCompleteCallback != null) {
            OnCompleteCallback();
        }
        CanActivateLevel(scene);
    }
}

public enum Level {
    EntryPoint,
    TestScene,
    LoginPage,
    RateMeScene,
    TestMap,
    TestGarage,
    TestRocket,
    MainMenu,
}
public enum LvlGroup {
    Menu,
    Shop,
    Game,
}

public interface ILevelAffector {
    void OnStartTransition(Level start, Level final);
    void OnLevelLoaded(Level level);
    void OnLevelActivated(Level level);
    bool CanActivateLevel(Level level);
    void Subscribe();
    void Unsubscribe();
}