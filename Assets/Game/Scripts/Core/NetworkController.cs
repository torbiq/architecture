﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon;
using System;

public class NetworkController : Singletone<NetworkController> {
    public string defaultRoomName = "123";

    public event Action OnJoinedRoomEvent;
    public event Action<PhotonPlayer> OnPlayerConnectedEvent;
    public event Action<PhotonPlayer> OnPlayerDisconnectedEvent;

    //public PhotonView photonView;
    
    protected override void Init() {
        if (PhotonNetwork.ConnectUsingSettings(Application.version)) {
            Log.Msg("Connected to Photon Network.");
        }
        else {
            Log.Error("Failed connection with Photon Network.");
        }
    }

    private void OnDestroy() {
        PhotonNetwork.Disconnect();
        Log.Msg("Disconnected from Photon Network.");
    }

    private void OnConnectedToMaster() {
        Log.Msg("Connected to Master Server.");
    }

    public void JoinGame() {
        var roomOptions = new RoomOptions() {
            IsVisible = false,
            MaxPlayers = 20,
        };
        //PhotonNetwork.sendRate = 10;
        //PhotonNetwork.sendRateOnSerialize = 64;
        PhotonNetwork.JoinOrCreateRoom(defaultRoomName,
            roomOptions,
            TypedLobby.Default);
        Log.Msg("Joined game.");
        //PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    private void OnJoinedRoom() {
        Log.Msg("Joined to room.");
        if (OnJoinedRoomEvent != null) {
            OnJoinedRoomEvent();
        }
    }
    
    private void OnPhotonPlayerConnected(PhotonPlayer player) {
        Log.Msg("Player connected.");
        if (OnPlayerConnectedEvent != null) {
            OnPlayerConnectedEvent(player);
        }
    }
    private void OnPhotonPlayerDisconnected(PhotonPlayer player) {
        Log.Msg("Player disconnected.");
        if (OnPlayerDisconnectedEvent != null) {
            OnPlayerDisconnectedEvent(player);
        }
    }

    private void OnFailedToConnectToPhoton() {
        Log.Error("Failed connection with Photon Network.");
    }
    private void OnDisconnectedFromPhoton() {
        Log.Msg("Disconnected from Photon Network.");
    }
}
