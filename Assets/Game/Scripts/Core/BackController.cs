﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackController : MonoBehaviour {
    public static BackController Instance { get; private set; }
    public bool IsQuitActive { get; private set; }
    public bool IsQuitDialogueTweenInProgress { get; private set; }

    public RectTransform exitPanelActivePos;
    public RectTransform exitPanelInactivePos;
    public RectTransform exitPanelRectTransform;
    public Image backgroundImage;
    public Text quitText;
    public Button yesButton;
    public Button noButton;
    public Button closeButton;
    public Button backgroundButton;

    private Dictionary<Level, Level?> _sceneSwitches = new Dictionary<Level, Level?> {
        { Level.EntryPoint, null },
        { Level.LoginPage, null },
        { Level.MainMenu, null },
        { Level.TestScene, Level.MainMenu },
        { Level.TestMap, Level.MainMenu },
        { Level.TestGarage, Level.MainMenu },
    };

    // Use this for initialization
    void Awake () {
        if (Instance) {
            if (Instance != this) {
                gameObject.SetActive(false);
                Destroy(gameObject);
                return;
            }
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        Init();
    }
    private void Init() {
        exitPanelRectTransform.anchoredPosition = exitPanelInactivePos.anchoredPosition;
        SetQuitButtonsStatus(true);
        backgroundImage.gameObject.SetActive(false);
        var color = backgroundImage.color;
        color.a = 0f;
        backgroundImage.color = color;
    }
    private void SetQuitButtonsStatus(bool status) {
        yesButton.interactable = status;
        noButton.interactable = status;
        closeButton.interactable = status;
        backgroundButton.interactable = status;
    }
    private void ShowQuitDialogue() {
        IsQuitDialogueTweenInProgress = true;
        SetQuitButtonsStatus(false);
        IsQuitActive = true;
        backgroundImage.DOKill(false);
        exitPanelRectTransform.DOAnchorPos(exitPanelActivePos.anchoredPosition, 0.5f);
        backgroundImage.gameObject.SetActive(true);
        backgroundImage.DOFade(0.5f, 0.5f).OnComplete(() => {
            IsQuitDialogueTweenInProgress = false;
            SetQuitButtonsStatus(true);
        });
    }
    private void HideQuitDialogue() {
        IsQuitDialogueTweenInProgress = true;
        SetQuitButtonsStatus(false);
        backgroundImage.DOKill(false);
        exitPanelRectTransform.DOAnchorPos(exitPanelInactivePos.anchoredPosition, 0.5f);
        backgroundImage.DOFade(0f, 0.5f).OnComplete(() => {
            IsQuitDialogueTweenInProgress = false;
            IsQuitActive = false;
            backgroundImage.gameObject.SetActive(false);
            SetQuitButtonsStatus(true);
        });
    }
    public void OnPressBack() {
        if (IsQuitDialogueTweenInProgress)
            return;
        if (IsQuitActive) {
            HideQuitDialogue();
        }
        else {
            Level? nextLevelFromDict;
            _sceneSwitches.TryGetValue(SceneController.GetCurrentLevelFromUnity(), out nextLevelFromDict);
            if (nextLevelFromDict == null) {
                ShowQuitDialogue();
            }
            else {
                SceneController.Instance.ChangeScene(nextLevelFromDict.Value);
            }
        }
    }
    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (!SceneController.Instance.IsInTransition()) {
                OnPressBack();
            }
        }
    }
}
