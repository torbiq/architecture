﻿using UnityEngine;
using System;

public class DataController : MonoBehaviour {
    private static DataController _instance;
    public static DataController Instance {
        get {
            if (_instance == null) {
                _instance = new GameObject().AddComponent<DataController>();
                _instance.name = "DataController";
                _instance.Load();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    private void Awake() {
        //HERE SHITS NEEDS TO BE GOING
    }

    [Serializable]
    public class PlayerData {
    
    }
    private static readonly string _playerDataKey = "PlayerDataJSON";
    private static readonly string pword = "+dwQcpUsnVrfFyjrgochBFWoWsYGl66+5p0Yox2a1nuEeB/yDbrdErDrrltrWPIpCHWs/Qo9SxyhjAMKd+ZGse94O56mBcVib3fOVvUMCpcAVnSdVAQ8vExW1g4C8dBqgsmEkeLTdhirjJYFH+DlMqsnk1T9i6B+pgeRL9GSXBJEPSaZr+4y1CiRSRdsnia+MFslL+ZFw6+29g4m22pNolU";
    
    public PlayerData currentPlayerData { get; private set; }
    private void Save() {
        SaveDataToPrefs(currentPlayerData);
    }
    public void Load(bool fromPrefs = true) {
        currentPlayerData = fromPrefs ? GetDataFromPrefs() : new PlayerData();
    }
    
    private static void SaveDataToPrefs(PlayerData playerData) {
        PlayerPrefs.SetString(_playerDataKey, AES.Encrypt(System.Text.Encoding.UTF8.GetBytes(JsonUtility.ToJson(playerData)), pword));
    }
    private static PlayerData GetDataFromPrefs() {
        var playerPrefsValue = PlayerPrefs.GetString(_playerDataKey, "");
        if (playerPrefsValue != "") {
            return JsonUtility.FromJson<PlayerData>(AES.Decrypt(playerPrefsValue, pword));
        }
        else {
            return new PlayerData();
        }
    }

    private void OnApplicationPause(bool pause) {
        if (pause) {
            Save();
        }
    }
    private void OnApplicationQuit() {
        Save();
    }
}