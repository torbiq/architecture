﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class EntryPoint : MonoBehaviour {
    private void Awake() {
        FB.Init(OnFBInitialized);
    }
    void OnFBInitialized () {
        FB.ActivateApp();
        if (FB.IsLoggedIn) {
            FB.LogOut();
        }
        SceneController.Instance.ChangeScene(Level.LoginPage);
	}
}
