﻿using System.Collections.Generic;
using UnityEngine.SceneManagement;

public static class BufferController {
    public enum BufferKey {

    }
    private static Dictionary<BufferKey, object> _temporaryInfo;
    private static Dictionary<BufferKey, object> _mainInfo;
    static BufferController() {
        _temporaryInfo = new Dictionary<BufferKey, object>();
        _mainInfo = new Dictionary<BufferKey, object>();

        SceneManager.sceneLoaded += OnSceneChangedHandler;
    }
    private static void OnSceneChangedHandler(Scene scene, LoadSceneMode loadSceneMode) {
        _temporaryInfo.Clear();
    }
    public static object GetMainDataByKey(BufferKey key) {
        object returnedInfo = null;
        _mainInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in main data of BufferController");
        }
        return returnedInfo;
    }
    public static object GetTemporaryDataByKey(BufferKey key) {
        object returnedInfo = null;
        _temporaryInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in temporary data of BufferController");
        }
        return returnedInfo;
    }
    public static T GetMainDataByKey<T>(BufferKey key) {
        object returnedInfo = null;
        _mainInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in main data of BufferController");
            return default(T);
        }
        return (T)returnedInfo;
    }
    public static T GetTemporaryDataByKey<T>(BufferKey key) {
        object returnedInfo = null;
        _temporaryInfo.TryGetValue(key, out returnedInfo);
        if (returnedInfo == null) {
            Log.Warning("Key [" + key + "] doesn't exist in temporary data of BufferController");
            return default(T);
        }
        return (T)returnedInfo;
    }
    public static void SetMainData(BufferKey key, object value) {
        if (_mainInfo.ContainsKey(key)) {
            _mainInfo[key] = value;
            Log.Warning("Key [" + key + "] info will be replaced in main data of BufferController");
            return;
        }
        _mainInfo.Add(key, value);
    }
    public static void SetTemporaryData(BufferKey key, object value) {
        if (_temporaryInfo.ContainsKey(key)) {
            _temporaryInfo[key] = value;
            Log.Warning("Key [" + key + "] info will be replaced in temporary data of BufferController");
            return;
        }
        _temporaryInfo.Add(key, value);
    }
    public static bool RemoveMainData(BufferKey key) {
        return _mainInfo.Remove(key);
    }
    public static bool RemoveTemporaryData(BufferKey key) {
        return _temporaryInfo.Remove(key);
    }
}