﻿using UnityEngine;
using System;
using DG.Tweening;
using System.Collections.Generic;
using Game.Extensions;
using System.Linq;

public abstract class BaseSceneController<T> : DestroyableSingletone<T> where T : BaseSceneController<T> {
    /// <summary>
    /// Previous frame's mouse position (same as Input.mousePosition).
    /// </summary>
    public Vector2? prevMouseScreenPos { get; private set; }
    /// <summary>
    /// Current mouse position (same as Input.mousePosition).
    /// </summary>
    public Vector2? currMouseScreenPos { get; private set; }

    /// <summary>
    /// Tweens to be destroyed when scene ends (like delayedcalls).
    /// </summary>
    private Stack<Tween> _destroyableTweens = new Stack<Tween>();
    /// <summary>
    /// Tweens to be destroyed when scene ends (like delayedcalls).
    /// </summary>
    public Stack<Tween> destroyableTweens {
        get { return _destroyableTweens; }
    }

    /// <summary>
    /// Current drag delta (between frames) in world coords. 
    /// </summary>
    public Vector2 currentDragDeltaVectorWorld { get; private set; }
    /// <summary>
    /// Current drag delta (between frames) in screen coords. 
    /// </summary>
    public Vector2 currentDragDeltaVectorScreen { get; private set; }

    /// <summary>
    /// Mouse delta in world coordinates since pressing mouse.
    /// </summary>
    public Vector2 dragDeltaVectorWorld { get; private set; }
    /// <summary>
    /// Mouse delta in screen coordinates since pressing mouse.
    /// </summary>
    public Vector2 dragDeltaVectorScreen { get; private set; }

    /// <summary>
    /// Overall mouse delta length in world coords since pressing mouse.
    /// </summary>
    public float dragDeltaLengthWorld { get; private set; }
    /// <summary>
    /// Overall mouse delta length in screen coords since pressing mouse.
    /// </summary>
    public float dragDeltaLengthScreen { get; private set; }

    /// <summary>
    /// Input.GetMouseButtonDown(0) shorthand.
    /// </summary>
    public bool mouseDown { get; private set; }
    /// <summary>
    /// Input.GetMouseButton(0) shorthand.
    /// </summary>
    public bool mouseDrag { get; private set; }
    /// <summary>
    /// Input.GetMouseButtonUp(0) shorthand.
    /// </summary>
    public bool mouseUp { get; private set; }

    /// <summary>
    /// Mouse delta in world coords.
    /// </summary>
    public Vector2 GetMouseDeltaWorld() {
        return prevMouseScreenPos == null || currMouseScreenPos == null ? Vector2.zero : (Vector2)(Camera.main.ScreenToWorldPoint(currMouseScreenPos.Value) - Camera.main.ScreenToWorldPoint(prevMouseScreenPos.Value));
    }
    /// <summary>
    /// Mouse delta in screen coords.
    /// </summary>
    public Vector2 GetMouseDeltaScreen() {

        return prevMouseScreenPos == null || currMouseScreenPos == null ? Vector2.zero : currMouseScreenPos.Value - prevMouseScreenPos.Value;
    }

    /// <summary>
    /// Touched collider by mouse position in world coords.
    /// </summary>
    public Collider2D TouchedColliderWorld() {
        return currMouseScreenPos == null ? null : Physics2D.OverlapPoint(Camera.main.ScreenToWorldPoint(currMouseScreenPos.Value));
    }
    /// <summary>
    /// Touched collider by mouse position in screen coords.
    /// </summary>
    public Collider2D TouchedColliderScreen() {
        return currMouseScreenPos == null ? null : Physics2D.OverlapPoint(currMouseScreenPos.Value);
    }

    /// <summary>
    /// Touched colliders by mouse position in world coords.
    /// </summary>
    public Collider2D[] TouchedCollidersWorld() {
        return Physics2D.OverlapPointAll(Camera.main.ScreenToWorldPoint(currMouseScreenPos.Value));
    }
    /// <summary>
    /// Touched colliders by mouse position in world coords.
    /// </summary>
    public Collider2D[] TouchedCollidersScreen() {
        return Physics2D.OverlapPointAll(currMouseScreenPos.Value);
    }

    /// <summary>
    /// Is collider touched by mouse position in world coords?
    /// </summary>
    public bool IsTouchingColliderWorld(Collider2D collider2d) {
        Collider2D[] touchedColliders = TouchedCollidersWorld();
        return touchedColliders.Contains(collider2d);
    }
    /// <summary>
    /// Is collider touched by mouse position in screen coords?
    /// </summary>
    public bool IsTouchingColliderScreen(Collider2D collider2d) {
        Collider2D[] touchedColliders = TouchedCollidersScreen();
        return touchedColliders.Contains(collider2d);
    }

    /// <summary>
    /// Default DOVirtual.DelayedCall() with centralized acess.
    /// </summary>
    /// <param name="time">Time in seconds.</param>
    /// <param name="callback">Callback used when time ends.</param>
    public Tween Wait(float time, TweenCallback callback) {
        var tween = DOVirtual.DelayedCall(time, callback, false);
        _destroyableTweens.Push(tween);
        return tween;
    }
    /// <summary>
    /// Speech delaying tween.
    /// </summary>
    private Tween _speechTween = null;
    /// <summary>
    /// Speech delaying tween.
    /// </summary>
    public Tween SpeechTween {
        get { return _speechTween; }
        set {
            KillSpeechDelay();
            _speechTween = value;
        }
    }
    /// <summary>
    /// Automatically sets SpeechTween (USE ONLY TO DELAY MAIN SPEECH OF CHARACTER).
    /// </summary>
    public Tween WaitToSay(float time, TweenCallback callback) {
        return SpeechTween = Wait(time, callback);
    }
    /// <summary>
    /// Kills speech tween.
    /// </summary>
    public void KillSpeechDelay() {
        _speechTween.KillIfPlaying();
    }
    /// <summary>
    /// Kills speech tween and stops speech.
    /// </summary>
    public void StopAndKillSpeech(float delayFade = 0f) {
        _speechTween.KillIfPlaying();
        //AudioController.Instance.StopSpeech(delayFade);
    }

    #region Mono update inherited
    protected override void Init() {
        SAwake();
        if (OnAwakeEvent != null) {
            OnAwakeEvent();
        }
    }
    private void Start() {
        SStart();
        if (OnStartEvent != null) {
            OnStartEvent();
        }
    }
    private void Update() {
        mouseDown = UnityEngine.Input.GetMouseButtonDown(0);
        mouseDrag = UnityEngine.Input.GetMouseButton(0);
        mouseUp = UnityEngine.Input.GetMouseButtonUp(0);
        
        if (mouseDown) {
            currMouseScreenPos = UnityEngine.Input.mousePosition;
        }

        if (mouseDrag) {
            prevMouseScreenPos = currMouseScreenPos;
            currMouseScreenPos = UnityEngine.Input.mousePosition;

            var deltaWorld = GetMouseDeltaWorld();
            var deltaScreen = GetMouseDeltaScreen();

            dragDeltaVectorWorld += deltaWorld;
            dragDeltaVectorScreen += deltaScreen;

            dragDeltaLengthScreen += deltaScreen.magnitude;
            dragDeltaLengthWorld += deltaWorld.magnitude;
        }

        SUpdate();
        if (OnUpdateEvent != null) {
            OnUpdateEvent();
        }

        if (mouseUp) {
            currMouseScreenPos = null;
            prevMouseScreenPos = null;

            dragDeltaVectorWorld = Vector2.zero;
            dragDeltaVectorScreen = Vector2.zero;

            dragDeltaLengthScreen = 0;
            dragDeltaLengthWorld = 0;
        }
    }
    private void OnDestroy() {
        while (destroyableTweens.Count != 0) {
            destroyableTweens.Pop().KillIfPlaying();
        }
        SOnDestroy();
        if (OnDestroyEvent != null) {
            OnDestroyEvent();
        }
    }
    #endregion

    public event Action OnAwakeEvent;
    public event Action OnStartEvent;
    public event Action OnUpdateEvent;
    public event Action OnDestroyEvent;

    /// <summary>
    /// Default Awake() to override in inherited class.
    /// </summary>
    virtual protected void SAwake() {
    }
    /// <summary>
    /// Default Start() to override in inherited class.
    /// </summary>
    virtual protected void SStart() {
    }
    /// <summary>
    /// Default Update() to override in inherited class.
    /// </summary>
    virtual protected void SUpdate() {
    }
    /// <summary>
    /// Default OnDestroy() to override in inherited class.
    /// </summary>
    virtual protected void SOnDestroy() {
    }
}
