﻿using System;
using System.Collections.Generic;
using UnityEngine;

public enum Age {
    AGE_FIVE_PLUS,
    AGE_TWO_PLUS,
    AGE_NO_RATING,
}

public static class AgeController {
    static AgeController() {
        AGE_PREF_KEY = "age_rating";
        _age = AgeStored;
    }
    public static readonly string AGE_PREF_KEY;
    public static Dictionary<Level, Age> ageOverrideScenes = new Dictionary<Level, Age> {
    };
    private static Age _age;
    private static readonly Age _defaultAge = Age.AGE_TWO_PLUS;
    private static Age AgeStored {
        get {
            return (Age)PlayerPrefs.GetInt(AGE_PREF_KEY, (int)_defaultAge);
        }
        set {
            PlayerPrefs.GetInt(AGE_PREF_KEY, (int)value);
        }
    }
    public static event Action<Age> OnAgeChangedEvent;
    public static bool IsAge(Age age) {
        Age currentAge = _age;
        ageOverrideScenes.TryGetValue(SceneController.GetCurrentLevelFromUnity(), out currentAge);
        return currentAge == age;
    }
    public static void SetAge(Age age) {
        if (ageOverrideScenes.ContainsKey(SceneController.GetCurrentLevelFromUnity())) {
            Log.Error("Can't change age for this level because it's overrided.");
            return;
        }
        if (!System.Enum.IsDefined(typeof(Age), age)) {
            Log.Error("Chosen age [value equals = " + age + "] is not defined in enum list.");
            return;
        }
        _age = age;
        AgeStored = _age;
        if (OnAgeChangedEvent != null) {
            OnAgeChangedEvent(_age);
        }
    }
    public static Age GetAge() {
        Age age;
        if (ageOverrideScenes.TryGetValue(SceneController.GetCurrentLevelFromUnity(), out age)) {
            return age;
        }
        return _age;
    }
}
