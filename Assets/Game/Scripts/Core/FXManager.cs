﻿using UnityEngine;
using System.Collections.Generic;

public enum FXName {
}

public static class FXManager {
    public static AudioClip GetClip(FXName fxName) {
        return AudioController.Instance.GetClip(fxName.ToString());
    }
}
