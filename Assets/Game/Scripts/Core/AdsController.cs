﻿#define TEST_ADS

using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using DG.Tweening;

public class AdsController : Singletone<AdsController>, ILevelAffector {

#if UNITY_ANDROID
    public static readonly string TEST_BANNER_ADMOB_ID = "ca-app-pub-3940256099942544/6300978111";
    public static readonly string TEST_INTER_ADMOB_ID = "ca-app-pub-3940256099942544/1033173712";
    public static readonly string TEST_REWARDED_ADMOB_ID = "ca-app-pub-3940256099942544/5224354917";

    public static readonly string real_banner_id = "";
    public static readonly string real_inter_id = "";
    public static readonly string real_rewarded_id = "";
#elif UNITY_IOS
    public static readonly string TEST_BANNER_ADMOB_ID = "ca-app-pub-3940256099942544/6300978111";
    public static readonly string TEST_INTER_ADMOB_ID = "ca-app-pub-3940256099942544/1033173712";
    public static readonly string TEST_REWARDED_ADMOB_ID = "ca-app-pub-3940256099942544/5224354917";

    public static readonly string real_banner_id = "";
    public static readonly string real_inter_id = "";
    public static readonly string real_rewarded_id = "";
#endif

    private string _banner_id;
    private string _inter_id;
    private string _rewarded_id;

    private float _interDelay = 60.0f;

    private Tween _interTween;

    private bool _isBannerReady;
    private bool _isInterReady;
    private bool _isRewardedReady;

    private bool _canShowInter;

    private BannerView _banner;
    private InterstitialAd _inter;
    private RewardBasedVideoAd _rewarded;

    [NonSerialized]
    public List<Level> bannerNotAllowed = new List<Level> {
        Level.EntryPoint,
        Level.LoginPage,
    };
    [NonSerialized]
    public List<Level> interNotAllowed = new List<Level> {
        Level.EntryPoint,
        Level.LoginPage,
    };
    //[NonSerialized]
    //public List<Level> rewardNotAllowed = new List<Level> {
    //    Level.EntryPoint,
    //    Level.LoginPage,
    //};
    
    protected override void Init() {
#if TEST_ADS
        _banner_id = TEST_BANNER_ADMOB_ID;
        _inter_id = TEST_INTER_ADMOB_ID;
        _rewarded_id = TEST_REWARDED_ADMOB_ID;
#else
        _banner_id = TEST_BANNER_ADMOB_ID;
        _inter_id = TEST_INTER_ADMOB_ID;
        _rewarded_id = TEST_REWARDED_ADMOB_ID;
#endif
        _banner = new BannerView(_banner_id, AdSize.SmartBanner, AdPosition.Bottom);
        _banner.OnAdLoaded += OnBannerLoadedHandler;
        _banner.LoadAd(new AdRequest.Builder().Build());

        InterstitialAd interAd = new InterstitialAd(_inter_id);
        interAd.OnAdLoaded += OnInterLoadedHandler;
        interAd.LoadAd(new AdRequest.Builder().Build());

        RewardBasedVideoAd rewardedAd = RewardBasedVideoAd.Instance;
        rewardedAd.OnAdLoaded += OnRewardedLoadedHandler;
        rewardedAd.LoadAd(new AdRequest.Builder().Build(), _rewarded_id);

        StartCanShowInterDelay(true);
    }

    private void OnBannerLoadedHandler(object sender, EventArgs e) {
        _isBannerReady = true;
    }
    private void OnInterLoadedHandler(object sender, EventArgs e) {
        _isInterReady = true;
    }
    private void OnRewardedLoadedHandler(object sender, EventArgs e) {
        _isRewardedReady = true;
    }

    private void SetBannerStatus(bool status) {
        if (status) {
            _banner.Show();
            return;
        }
        _banner.Hide();
    }
    private void ShowInter() {
        _inter.Show();
    }
    private void ShowRewarded() {
        _rewarded.Show();
    }

    public void OnStartTransition(Level start, Level final) {
        SetBannerStatus(false);
    }

    public void OnLevelLoaded(Level level) {
    }

    public void OnLevelActivated(Level level) {
        SetBannerStatus(!bannerNotAllowed.Contains(level));
    }

    public bool CanActivateLevel(Level level) {
        if (_isInterReady && _canShowInter) {
            _canShowInter = false;
            _inter.OnAdClosed += OnInterClosed;
            ShowInter();
            return false;
        }
        return true;
    }

    private void SetCanShowInter(bool val) {
        _interTween.Kill(false);
        _canShowInter = val;
    }

    private void StartCanShowInterDelay(bool endVal) {
        _interTween.Kill(false);
        _interTween = DOVirtual.DelayedCall(_interDelay, () => {
            _canShowInter = endVal;
        }, true);
    }

    private void OnInterClosed(object sender, EventArgs e) {
        SetCanShowInter(false);
        StartCanShowInterDelay(true);
        _inter.OnAdClosed -= OnInterClosed;
        SceneController.Instance.CanActivateLevel(SceneController.Instance.GetCurrentLevel());
    }
    
    public void Subscribe() {
        SceneController.Instance.OnStartTransition += OnStartTransition;
        SceneController.Instance.OnLevelLoaded += OnLevelLoaded;
        SceneController.Instance.OnLevelActivated += OnLevelActivated;
    }

    public void Unsubscribe() {
        SceneController.Instance.OnStartTransition -= OnStartTransition;
        SceneController.Instance.OnLevelLoaded -= OnLevelLoaded;
        SceneController.Instance.OnLevelActivated -= OnLevelActivated;
    }
}
