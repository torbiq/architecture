﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : Singletone<PauseController> {
    public float lastSavedTimeScale { get; set; }

    public event Action OnPause;
    public event Action OnUnpause;

    protected override void Init() {
        lastSavedTimeScale = Time.timeScale;
    }
    public void Pause() {
        lastSavedTimeScale = Time.timeScale;
        Time.timeScale = 0f;
        if (OnPause != null) {
            OnPause();
        }
    }
    public void Unpause() {
        Time.timeScale = lastSavedTimeScale;
        if (OnUnpause != null) {
            OnUnpause();
        }
    }
}
