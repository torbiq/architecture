﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonScene : MonoBehaviour {
    public Level level;
    private Button _button;
    private void Awake() {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
    }
    private void OnClick() {
        if (BackController.Instance && _button.interactable) {
            SceneController.Instance.ChangeScene(level);
        }
    }
}

