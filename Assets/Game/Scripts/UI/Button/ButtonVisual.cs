﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonVisual : MonoBehaviour {
    public Vector3 scalingAmount = new Vector3(0.1f, 0.1f, 0.1f);
    public float duration = 1f;
    public int vibrato = 10;
    public float elasticity = 1f;
    public Ease ease = Ease.InElastic;
    private RectTransform _rt;
    private Tween _punchTween;
    private Button _button;
    public void Awake() {
        _rt = GetComponent<RectTransform>();
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
    }
    private void OnClick() {
        if (_button.interactable) {
            _punchTween.Kill(true);
            _punchTween = ((Transform)_rt).DOPunchScale(scalingAmount, duration, vibrato, elasticity).SetEase(ease);
        }
    }
}

