﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonSound : MonoBehaviour {
    public AudioClip clip;
    private Button _button;
    private void Awake() {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
    }
    private void OnClick() {
        if (_button.interactable && AudioController.Instance) {
            AudioController.Instance.Play(clip);
        }
    }
}
