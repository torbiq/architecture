﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ButtonBack : MonoBehaviour {
    private Button _button;
    private void Awake() {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
    }
    private void OnClick() {
        if (BackController.Instance && _button.interactable) {
            BackController.Instance.OnPressBack();
        }
    }
}