﻿using Game.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class TestGarageCamera : MonoBehaviour {
    public Animator characterAnimator;

    public Transform horizontalRotatorTransform;
    public Transform verticalRotatorTransform;

    public Vector2 maxSpeed;
    public Vector2 acceleration;
    public Vector2 deAcceleration;

    public float maxY;
    public float minY;

    private Vector2 _currentSpeed;
    private Vector2 _prevMousePos;
    private Vector2 _currentMousePos;

    private Camera _camera;

    private void Awake() {
        _camera = GetComponent<Camera>();
        characterAnimator.Play("garage_idle", 0, 0f);
    }

    private Vector2 DownScaleVector2(Vector2 value, Vector2 howMuch) {
        float x = value.x;
        float y = value.y;

        if (x > 0f) {
            x = Mathf.Max(0f, x - howMuch.x);
        }
        else if (x < 0f) {
            x = Mathf.Min(0f, x + howMuch.x);
        }
        if (y > 0f) {
            y = Mathf.Max(0f, y - howMuch.y);
        }
        else if (y < 0f) {
            y = Mathf.Min(0f, y + howMuch.y);
        }

        return new Vector2(x, y);
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            _currentSpeed = Vector2.zero;
            _currentMousePos = Input.mousePosition;
        }
        if (Input.GetMouseButton(0)) {
            _prevMousePos = _currentMousePos;
            _currentMousePos = Input.mousePosition;

            Vector2 mouseSpeed = (_currentMousePos - _prevMousePos) / Time.deltaTime;
            Log.Msg("Mouse speed: " + mouseSpeed);
            Vector2 screenDeltaRelative = new Vector2(mouseSpeed.x / (float)Screen.width, mouseSpeed.y / (float)Screen.height);
            Vector2 speedDelta = acceleration;
            speedDelta.Scale(screenDeltaRelative);
            Log.Msg("Screen relative delta: " + screenDeltaRelative);
            Log.Msg("Speed relative screen delta: " + speedDelta);

            _currentSpeed += speedDelta;
            _currentSpeed = new Vector2(EMath.Normalize(_currentSpeed.x, -maxSpeed.x, maxSpeed.x), EMath.Normalize(_currentSpeed.y, -maxSpeed.y, maxSpeed.y));
        }
        Vector2 downSpeedDelta = Time.deltaTime * deAcceleration;
        _currentSpeed = DownScaleVector2(_currentSpeed, downSpeedDelta);
        //downSpeedDelta = new Vector2(EMath.Normalize(_currentSpeed.x, -maxSpeed.x, maxSpeed.x), EMath.Normalize(_currentSpeed.y, -maxSpeed.y, maxSpeed.y));

        horizontalRotatorTransform.Rotate(Vector3.up, _currentSpeed.x * Time.deltaTime);
        verticalRotatorTransform.Rotate(Vector3.right, _currentSpeed.y * Time.deltaTime);

        var rot = verticalRotatorTransform.localRotation;
        rot.x = EMath.Normalize(rot.x, minY / 360f, maxY / 360f);
        verticalRotatorTransform.localRotation = rot;

        //float nextXVerticalRotation = verticalRotatorTransform.rotation.x + _currentSpeed.y * Time.deltaTime;
        ////nextXVerticalRotation = EMath.Normalize(nextXVerticalRotation, minY, maxY);


        //var verticalRotation = verticalRotatorTransform.rotation;
        //verticalRotation.x = nextXVerticalRotation;
        //verticalRotatorTransform.rotation = verticalRotation;
    }
}
