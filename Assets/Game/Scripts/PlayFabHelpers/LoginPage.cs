﻿using PlayFab;
using PlayFab.ClientModels;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using DG.Tweening;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class LoginPage : MonoBehaviour {

    // Login form.
    public InputField logUsrField;
    public InputField logPwordField;
    public Button logLoginButton;
    public Button logLoginFacebookButton;
    public Button logLoginDeviceIDButton;
    public Text logErrorText;

    // Registration form.
    public InputField regUsrField;
    public InputField regEmailField;
    public InputField regPwordField;
    public Button regRegisterButton;
    public Text regErrorText;

    public Image inputBlockerImage;

    private void OnLoggedIn() {
        SceneController.Instance.ChangeScene(Level.MainMenu);
    }

    private void Awake() {
        var color = inputBlockerImage.color;
        color.a = 0f;
        inputBlockerImage.color = color;
        inputBlockerImage.gameObject.SetActive(false);

        logLoginButton.onClick.AddListener(TryLoginCustomAcc);
        logLoginFacebookButton.onClick.AddListener(TryLoginWithFacebook);
        logLoginDeviceIDButton.onClick.AddListener(TryLoginWithDeviceID);

        regRegisterButton.onClick.AddListener(TryRegister);

        logUsrField.onEndEdit.AddListener((str) => { logPwordField.Select(); });
        logPwordField.onEndEdit.AddListener((str) => {
            var data = new PointerEventData(EventSystem.current);
            logLoginButton.OnPointerClick(data);
        });

        regEmailField.onEndEdit.AddListener((str) => { regUsrField.Select(); });
        regUsrField.onEndEdit.AddListener((str) => { regPwordField.Select(); });
        regPwordField.onEndEdit.AddListener((str) => {
            var data = new PointerEventData(EventSystem.current);
            regRegisterButton.OnPointerClick(data);
        });
    }

    private void OnStartAction() {
        inputBlockerImage.DOKill(false);
        inputBlockerImage.gameObject.SetActive(true);
        inputBlockerImage.DOFade(0.5f, 0.5f);
    }
    private void OnEndAction() {
        inputBlockerImage.DOKill(false);
        inputBlockerImage.DOFade(0.0f, 0.5f).OnComplete(() => {
            inputBlockerImage.gameObject.SetActive(false);
        });
    }
    private void TryLoginCustomAcc() {
        OnStartAction();
        var req = new LoginWithPlayFabRequest();
        req.Username = logUsrField.text;
        req.Password = logPwordField.text;
        req.TitleId = PlayFabSettings.TitleId;
        PlayFabClientAPI.LoginWithPlayFab(req,
            res => {
                OnLoggedIn();
            },
            err => {
                logErrorText.text = err.ErrorMessage;
                OnEndAction();
            });
    }
    private void TryLoginWithFacebook() {
        OnStartAction();
        FB.LogInWithReadPermissions(new List<string> { "public_profile" }, callback: (loginFBresult) => {
            if (!string.IsNullOrEmpty(loginFBresult.Error)) {
                logErrorText.text = loginFBresult.Error;
                OnEndAction();
                return;
            }
            if (loginFBresult.Cancelled) {
                logErrorText.text = "Facebook login cancelled.";
                OnEndAction();
                return;
            }
            var req = new LoginWithFacebookRequest();
            req.AccessToken = loginFBresult.AccessToken.TokenString;
            req.CreateAccount = true;
            req.TitleId = PlayFabSettings.TitleId;
            PlayFabClientAPI.LoginWithFacebook(req,
                (res) => {
                    OnLoggedIn();
                },
                (err) => {
                    logErrorText.text = err.ErrorMessage;
                    OnEndAction();
                });
        });
    }
    private void TryLoginWithDeviceID() {
        OnStartAction();
#if UNITY_ANDROID
        var req = new LoginWithAndroidDeviceIDRequest();
        req.AndroidDevice = SystemInfo.deviceModel;
        req.AndroidDeviceId = SystemInfo.deviceUniqueIdentifier;
        req.CreateAccount = true;
        req.OS = SystemInfo.operatingSystem;
        req.TitleId = PlayFabSettings.TitleId;
        PlayFabClientAPI.LoginWithAndroidDeviceID
#elif UNITY_IOS
        var req = new LoginWithIOSDeviceIDRequest();
        req.DeviceModel = SystemInfo.deviceModel;
        req.DeviceId = SystemInfo.deviceUniqueIdentifier;
        req.CreateAccount = true;
        req.OS = SystemInfo.operatingSystem;
        req.TitleId = PlayFabSettings.TitleId;
        PlayFabClientAPI.LoginWithIOSDeviceID
#endif
        (req,
        res => {
            OnLoggedIn();
        },
        err => {
            logErrorText.text = err.ErrorMessage;
            OnEndAction();
        });
    }
    private void TryRegister() {
        OnStartAction();
        var req = new RegisterPlayFabUserRequest();
        req.TitleId = PlayFabSettings.TitleId;
        req.Username = regUsrField.text;
        req.Password = regPwordField.text;
        req.Email = regEmailField.text;
        //req.RequireBothUsernameAndEmail = true;
        PlayFabClientAPI.RegisterPlayFabUser(req,
            res => {
                regErrorText.text = "Registration succes. Now you can login using that username & password.";
                OnEndAction();
                logUsrField.text = regUsrField.text;
                logPwordField.text = regPwordField.text;
                TryLoginCustomAcc();
            },
            err => {
            regErrorText.text = err.ErrorMessage;
            Debug.LogError(err.Error.ToString());
            Debug.LogError(err.ErrorMessage);
                foreach (var detailedMsg in err.ErrorDetails) {
                    Debug.LogError("[Key]: " +  detailedMsg.Key);
                    foreach (var msg in detailedMsg.Value) {
                        Debug.LogError(msg);
                    }
                }
                OnEndAction();
            });
    }
}
