﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Rocket : MonoBehaviour, ITrackMovable {
    public bool isActive { get; private set; }
    public GameObject model;
    public ParticleSystem rocketFlame;
    public ParticleSystem explosion;
    public float velocity;
    public float hitRange;

    public float currentPosition { get; private set; }
    public SimpleTrack track { get; private set; }
    public int trackIndex { get; set; }

    public void Launch(SimpleTrack track, int trackIndex, float currentPosition) {
        this.track = track;
        this.trackIndex = trackIndex;
        this.currentPosition = currentPosition;
        gameObject.SetActive(true);
        rocketFlame.Play();
        isActive = true;
    }

    // Using FixedUpdate to dont affect physics on low or high fps
    public void FixedUpdate() {
        float delta = velocity * Time.fixedDeltaTime;
        Transform thisTransform = transform;
        float currentPosition = this.currentPosition;
        track.ChangePosition(ref currentPosition, delta, ref thisTransform);
        this.currentPosition = currentPosition;
        track.ChangeOffset(trackIndex, ref thisTransform);
    }

    public void Explode() {
        isActive = false;
        model.SetActive(false);
        rocketFlame.Stop();
        explosion.Play();
        Destroy(gameObject, explosion.main.duration);
        enabled = false;
    }
}

public interface ITrackMovable {
    float currentPosition { get; }
    int trackIndex { get; set; }
    SimpleTrack track { get; }
}
