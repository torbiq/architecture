﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Curve;
using UnityEngine.UI;
using DG.Tweening;
using Game.Extensions;

public class GameController : DestroyableSingletone<GameController> {
    public int neededLaps;
    public GameObject dummy;
    public SimpleTrack track;
    public Text counterText;

    public Dictionary<int, GameObject> actIdToGoDict { get; private set; }
    public int madeLaps { get; set; }
    public float timePast { get; private set; }
    public bool isGameStarted { get; private set; }

    protected override void Init() {
        var transpTextCol = counterText.color;
        transpTextCol.a = 0f;
        
        counterText.color = transpTextCol;

        madeLaps = 0;
        timePast = 0;
        isGameStarted = false;

        actIdToGoDict = new Dictionary<int, GameObject>();

        NetworkController.Instance.OnPlayerConnectedEvent += OnPlayerConnectedHandler;
        NetworkController.Instance.OnPlayerDisconnectedEvent += OnPlayerDisconnectedHandler;
        NetworkController.Instance.OnJoinedRoomEvent += OnJoinedRoomHandler;
    }

    private void Start() {
        NetworkController.Instance.JoinGame();

        //StartGame();

        float step = 2;
        float distanceToCover = track.trackLength;
        while (distanceToCover > 0f) {
            for (int i = 0; i < track.count; i++) {
                var dummyInstance = GameObject.Instantiate(dummy, dummy.transform.parent);
                dummyInstance.SetActive(true);
                Transform transfDummy = dummyInstance.transform;
                track.PlaceOn(distanceToCover, ref transfDummy, i);
            }
            distanceToCover -= step;
        }
    }

    private void OnJoinedRoomHandler() {
        Log.Msg("Joind room handler.");
        var instance = PhotonNetwork.Instantiate("Prefabs/Characters/Firefly", Vector3.zero, Quaternion.identity, 0);
        if (PhotonNetwork.countOfPlayers >= 2) {
            DOVirtual.DelayedCall(2f, () => {
                StartGame();
            });
        }
    }

    private void OnPlayerConnectedHandler(PhotonPlayer player) {
        Log.Msg("Player connected handler.");
        var instance = PhotonNetwork.Instantiate("Prefabs/Characters/Firefly", Vector3.zero, Quaternion.identity, 0);
        actIdToGoDict.Add(player.ID, instance);
        if (PhotonNetwork.countOfPlayers >= 2) {
            DOVirtual.DelayedCall(2f, () => {
                StartGame();
            });
        }
    }

    private void OnPlayerDisconnectedHandler(PhotonPlayer player) {
        var go = actIdToGoDict[player.ID];
        Destroy(go);
        actIdToGoDict.Remove(player.ID);
    }

    private void Update() {
        if (isGameStarted) {
            timePast += Time.deltaTime;
        }
    }
    
    private Tween ShowCounter(string text) {
        counterText.text = text;
        counterText.transform.localScale = Vector3.zero;
        var color = counterText.color;
        color.a = 0f;
        counterText.color = color;
        counterText.DOFade(1f, 0.2f).OnComplete(() => {
            DOVirtual.DelayedCall(0.2f, () => {
                counterText.DOFade(0f, 0.6f);
            }, false);
        });
        return counterText.transform.DOScale(1f, 1f);
    }

    private void StartGame() {
        ShowCounter("3").OnComplete(() => {
            ShowCounter("2").OnComplete(() => {
                ShowCounter("1").OnComplete(() => {
                    ShowCounter("GO!");
                    isGameStarted = true;
                });
            });
        });
    }

    private void OnDestroy() {
        PhotonNetwork.LeaveRoom();

        NetworkController.Instance.OnPlayerConnectedEvent -= OnPlayerConnectedHandler;
        NetworkController.Instance.OnPlayerDisconnectedEvent -= OnPlayerDisconnectedHandler;
        NetworkController.Instance.OnJoinedRoomEvent -= OnJoinedRoomHandler;
    }
}
