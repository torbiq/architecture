﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BansheeGz.BGSpline.Curve;
using BansheeGz.BGSpline.Components;
using Game.Extensions;

public class SimpleTrack : MonoBehaviour {
    //  |RTrack>|
    //  0 <Dis> 1
    //  |<LTrack|
    public float distance = 1.5f;
    public int count = 5;
    //  | | | | |
    //  0 1 P 3 4
    //  | | | | |
    public int primaryIndex = 2;
    public float[] optimizedOffsets { get; private set; }
    public BGCurve curve;
    public BGCcCursor cursor;

    public float trackLength { get; private set; }

    private void Awake() {
        trackLength = cursor.Math.GetDistance();
        optimizedOffsets = new float[count];
        for (int i = 0; i < count; i++) {
            optimizedOffsets[i] = (i - primaryIndex) * distance;
        }
    }
    public float GetTrackOffset(int index) {
        return optimizedOffsets[index];
    }
    public void ChangePosition(ref float currentCursorPos, float newDeltaDistance, ref Transform transform) {
        currentCursorPos = EMath.Cycled(currentCursorPos + newDeltaDistance, 0f, trackLength);
        cursor.Distance = currentCursorPos;

        Vector3 newWorldPosition = cursor.CalculatePosition();
        transform.position = newWorldPosition;
        Vector3 newWorldLookDirection = cursor.CalculateTangent();
        Quaternion rotationQuaternion = transform.rotation;
        rotationQuaternion.SetLookRotation(newWorldLookDirection);
        transform.rotation = rotationQuaternion;
    }
    public void PlaceOn(float cursorPos, ref Transform transform, int trackIndex) {
        cursorPos = EMath.Cycled(cursorPos, 0f, trackLength);
        cursor.Distance = cursorPos;
        transform.position = cursor.CalculatePosition();
        Vector3 newWorldLookDirection = cursor.CalculateTangent();
        Quaternion rotationQuaternion = transform.rotation;
        rotationQuaternion.SetLookRotation(newWorldLookDirection);
        transform.rotation = rotationQuaternion;
        ChangeOffset(trackIndex, ref transform);
    }
    public void ChangeOffset(int trackIndex, ref Transform transform) {
        transform.position += transform.TransformDirection(Vector3.right) * GetTrackOffset(trackIndex);
    }
}
