﻿using Game.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacingInputController : BaseSceneController<RacingInputController> {
    private float _minScreenDeltaToTurn = 0.2f;
    private float _currentMouseOverallDeltaX;
    private float _deltaPartOfScreenNeededToChangeIndex = 0.2f;
    
	// Update is called once per frame
	protected override void SUpdate () {
        if (RacingCharacterController.mine) {
            if (mouseDown) {
                _currentMouseOverallDeltaX = 0f;
            }
            if (mouseDrag) {
                float currScreenDeltaX = GetMouseDeltaScreen().x;
                _currentMouseOverallDeltaX += currScreenDeltaX;
            }
            if (mouseUp) {
                float screenPercentDelta = (_currentMouseOverallDeltaX / Screen.width);
                _currentMouseOverallDeltaX = 0f;
                if (Mathf.Abs(screenPercentDelta) >= _minScreenDeltaToTurn) {
                    if (screenPercentDelta > 0) {
                        RacingCharacterController.mine.MoveRight();
                    }
                    else {
                        RacingCharacterController.mine.MoveLeft();
                    }
                    //int prevTrackIndex = RacingCharacterController.mine.trackIndex;
                    //RacingCharacterController.mine.trackIndex = EMath.Normalize(RacingCharacterController.mine.trackIndex + indexDelta, 0, RacingCharacterController.mine.track.count - 1);
                    //int realDelta = RacingCharacterController.mine.trackIndex - prevTrackIndex;
                    //RacingCharacterController.mine.currentXOffset -= realDelta * RacingCharacterController.mine.track.distance;
                }
            }
        }
    }
}
