﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AInput;
using BansheeGz.BGSpline.Curve;
using BansheeGz.BGSpline.Components;
using Game.Extensions;
using System;

//public enum HeroState {
//    Idle,
//    Start,
//    Run,
//    TurnL,
//    TurnR,
//    Acceleration,
//    Braking,
//    Reaload,

//    RocketShtorm,
//    BombPlant,
//    Damage,
//    FinishStop,
//    Victory,

//    GarageIdle,
//}

public class RacingCharacterController : MonoBehaviour, ITrackMovable {
    public float maxSpeed = 5f;
    public float accelRate = 0.5f;
    public float downSpeedRate = 0.6f;
    public float xSpeed = 0.6f;
    private PhotonView _photonView;

    public static RacingCharacterController mine { get; private set; }

    public Animator characterAnimator;

    private float _currentSpeed;
    public float currentXOffset { get; set; }

    public float currentPosition { get; private set; }
    public SimpleTrack track { get; private set; }
    public int trackIndex { get; set; }

    public float GetSpeed() {
        return _currentSpeed;
    }

    private void Awake() {
        _photonView = GetComponent<PhotonView>();
        var camera = transform.Find("Main Camera").GetComponent<Camera>();
        _currentSpeed = 0f;
        if (_photonView.isMine) {
            camera.gameObject.SetActive(true);
            mine = this;
        }
        else {
            camera.gameObject.SetActive(false);
        }
    }

    private void Start() {
        track = GameController.Instance.track;
        trackIndex = track.primaryIndex;
    }

    private void UpdateOffset() {
        if (currentXOffset > 0) {
            currentXOffset = Mathf.Max(0, currentXOffset - Time.deltaTime * xSpeed);
        }
        else {
            currentXOffset = Mathf.Min(0, currentXOffset + Time.deltaTime * xSpeed);
        }
    }

    private bool _tempIsGoingBack;

    private void SetTurnBooleans() {
        // U divided By U/S = U/U/S = U*S/U = S 
        float goBackDuration = Mathf.Abs(currentXOffset) / xSpeed;
        float animLength = characterAnimator.GetClipLength("turnR");
        bool isFasterThanAnimation = goBackDuration <= animLength;
        bool isTurningR = characterAnimator.GetBool("isTurningRight");
        bool isTurningL = characterAnimator.GetBool("isTurningLeft");

        if (_tempIsGoingBack) {
            _tempIsGoingBack = currentXOffset != 0f;
            return;
        }
        if (isTurningR && isFasterThanAnimation) {
            isTurningR = false;
            _tempIsGoingBack = true;
        }
        if (isTurningL && isFasterThanAnimation) {
            isTurningL = false;
            _tempIsGoingBack = true;
        }
        isTurningL = currentXOffset > 0f;
        isTurningR = currentXOffset < 0f;

        characterAnimator.SetBool("isTurningRight", isTurningR);
        characterAnimator.SetBool("isTurningLeft", isTurningL);
    }

    public void MoveLeft() {
        int prevTrackIndex = trackIndex;
        trackIndex = EMath.Normalize(trackIndex - 1, 0, track.count - 1);
        int realDelta = trackIndex - prevTrackIndex;
        currentXOffset -= realDelta * track.distance;
    }

    public void MoveRight() {
        int prevTrackIndex = trackIndex;
        trackIndex = EMath.Normalize(trackIndex + 1, 0, track.count - 1);
        int realDelta = trackIndex - prevTrackIndex;
        currentXOffset -= realDelta * track.distance;
    }
    
    private void InputMovement() {
        float deltaOnTrack = _currentSpeed * Time.deltaTime;
        Transform thisTransform = transform;
        float currentPosition = this.currentPosition;
        track.ChangePosition(ref currentPosition, deltaOnTrack, ref thisTransform);
        this.currentPosition = currentPosition;
        track.ChangeOffset(trackIndex, ref thisTransform);
        transform.position += transform.TransformDirection(Vector3.right) * currentXOffset;

        UpdateOffset();
    }
    
    private void Update() {
        if (_photonView.isMine) {
            if (GameController.Instance.isGameStarted) {
                if (!characterAnimator.GetBool("isFlying")) {
                    characterAnimator.SetBool("isFlying", true);
                }
                _currentSpeed = EMath.Normalize(_currentSpeed + accelRate * Time.deltaTime, 0f, maxSpeed);
            }

            SetTurnBooleans();

            InputMovement();
        }
    }
}
