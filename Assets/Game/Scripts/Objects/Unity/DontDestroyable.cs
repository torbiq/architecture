﻿using UnityEngine;
using System.Collections;

public class DontDestroyableOnLoad : MonoBehaviour {
    protected void Awake() {
        DontDestroyOnLoad(gameObject);
    }
}
