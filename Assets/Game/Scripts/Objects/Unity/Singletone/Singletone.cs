﻿using UnityEngine;

public abstract class Singletone<T> : DontDestroyableOnLoad where T : Singletone<T> {
    private static T _instance;
    public static T Instance {
        get {
            return _instance;
        }
    }
    protected new void Awake() {
        if (_instance != null) {
            if (_instance != this) {
                Log.Warning("Singletone of type " + typeof(T).ToString() + " already exists. Destroying gameObject.");
                gameObject.SetActive(false);
                DestroyImmediate(gameObject);
                return;
            }
        }
        _instance = (T)this;
        base.Awake();
        Init();
    }
    protected abstract void Init();
}
