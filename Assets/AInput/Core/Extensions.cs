﻿using DG.Tweening;
using UnityEngine;

namespace AInput.Extensions
{
    public static class TweenExtensions {
        public static Tween DOFade(this TextMesh textMesh, float endValue, float duration) {
            var endColor = textMesh.color;
            endColor.a = endValue;
            return DOTween.To(() => { return textMesh.color; }, x => textMesh.color = x, endColor, duration);
        }
        public static Tween DOColor(this TextMesh textMesh, Color endValue, float duration) {
            return DOTween.To(() => { return textMesh.color; }, x => textMesh.color = x, endValue, duration);
        }
        /// <summary>
        /// Kills tween if it's playing.
        /// </summary>
        public static void KillIfPlaying(this Tween tween, bool complete = false) {
            if (tween != null) {
                if (tween.IsPlaying()) {
                    tween.Kill(complete);
                }
            }
        }
    }
}